package com.example.demo.Admin;

import com.example.demo.controllers.AdminController;
import com.example.demo.domains.School;
import com.example.demo.entities.SchoolEntity;
import com.example.demo.repositories.SchoolRepository;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ActiveProfiles("integrationtest")
class AdminControllerTest {
    @LocalServerPort
    int port;

    @Autowired AdminController adminController;
    @Autowired SchoolRepository schoolRepository;

    @Test
    public void test_Create_School(){
        WebClient webClient = WebClient.create("http://localhost:"+port);
        SchoolEntity schoolEntity = new SchoolEntity("56","ChristianSkolan","email","Stocholm",new ArrayList<>());


        School school = webClient.post()
                .uri("/admin/createSchool")
                .header(MediaType.APPLICATION_JSON_VALUE)
                .body(Mono.just(schoolEntity),SchoolEntity.class)
                .retrieve()
                .bodyToMono(School.class)
                .block();

        assertEquals(schoolEntity.getName(),school.getName());
    }

}