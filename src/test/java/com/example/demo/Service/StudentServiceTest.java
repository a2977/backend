package com.example.demo.Service;

import com.example.demo.dto.CreateCompany;
import com.example.demo.entities.CompanyEntity;
import com.example.demo.entities.StudentEntity;
import com.example.demo.exceptions.CompanyException;
import com.example.demo.exceptions.StudentException;
import com.example.demo.repositories.CompanyRepository;
import com.example.demo.repositories.FileDBRepository;
import com.example.demo.repositories.StudentCompanyRepository;
import com.example.demo.repositories.StudentRepository;
import com.example.demo.services.CompanyService;
import com.example.demo.services.StudentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)

public class StudentServiceTest {

    @Mock
    StudentRepository studentRepository;
    @Mock
    CompanyRepository companyRepository;
    @Mock
    StudentCompanyRepository studentCompanyRepository;

    @Mock
    StudentService studentService;


    StudentEntity diako;
    StudentEntity dan;
    StudentEntity dennise;

    CompanyEntity comp1;
    CompanyEntity comp2;

    StudentEntity studentExpected;

    @BeforeEach
    void setUp() {
        diako = new StudentEntity("1", "Diako", "diaoko@email", "Vås", "Java", "Ec", "imgurl", null);
        dan = new StudentEntity("2", "Dan", "Dan@email", "Uppsala", "Java", "Ec", "imgurl", null);
        dennise = new StudentEntity("3", "Dennise", "Den@email", "Uppsala", "Java", "Ec", "imgurl", null);

        comp1 = new CompanyEntity("1", "Niclas", "niclas@gmail.com", "java", "stockholm", null);
        comp2 = new CompanyEntity("2", "Oscar", "niclas@gmail.com", "java", "stockholm", null);

        studentExpected = new StudentEntity("2", "Dan", "Dan@email", "Uppsala", "Java", "Ec", "1", null);
    }

    @Test
    void itShould_Return_StreamOfStudents() {
        when(studentService.allStudents()).thenReturn(Stream.of(diako, dan, dennise));
       List<StudentEntity> students = studentService.allStudents().collect(Collectors.toList());

        assertEquals(3, students.size());
    }

    @Test
    void itShould_Return_SingleStudents() {
        //Given
        when(studentService.allStudents()).thenReturn(Stream.of(diako, dan, dennise));

        List<StudentEntity> students = studentService.allStudents().filter(i -> i.getName().equals("Diako")).collect(Collectors.toList());
        System.out.println(students);

        //Then
        assertEquals(1, students.size());
        assertThat(students).anySatisfy(studentEntity -> {
            assertThat(studentEntity.getId()).isEqualTo("1");
            assertThat(studentEntity.getName()).isEqualTo("Diako");
            assertThat(studentEntity.getEmail()).isEqualTo("diaoko@email");
            assertThat(studentEntity.getCity()).isEqualTo("Vås");
            assertThat(studentEntity.getCourse()).isEqualTo("Java");
            assertThat(studentEntity.getSchool()).isEqualTo("Ec");
            assertThat(studentEntity.getImageId()).isEqualTo("imgurl");
        });

        assertEquals(diako, students.get(0));
    }


    @Test
    void itShould_SearchCompanies_by_name_or_city() {
        //Given
        //When
        when(studentService.searchCompanies("Niclas")).thenReturn(Stream.of(comp1));

        List<CompanyEntity> companyActual = studentService.searchCompanies("Niclas").collect(Collectors.toList());
        System.out.println(companyActual);
        //Then
        assertEquals(1, companyActual.size());
        assertEquals(comp1, companyActual.get(0));

    }

    @Test
    void itShould_Return_Single_Student_ById() throws StudentException {
        //Given
        //When
        when(studentService.getById("2")).thenReturn(dan);
        StudentEntity studentActual = studentService.getById("2");

        //Then
        assertEquals(dan, studentActual);


    }

    @Test
    void itShould_add_companyToStudent() throws StudentException, CompanyException {
        //Given
        companyRepository.save(comp1);
        studentRepository.save(dan);

        //When
        when(studentService.addToFavorites("2", "1")).thenReturn(studentExpected);
        StudentEntity studentActual = studentService.addToFavorites("2", "1");

        //Then
        assertEquals(studentExpected.getImageId(), studentActual.getImageId());
    }


}
