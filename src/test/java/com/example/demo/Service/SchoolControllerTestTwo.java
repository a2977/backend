package com.example.demo.Service;

import com.example.demo.domains.Company;
import com.example.demo.domains.School;
import com.example.demo.domains.Student;
import com.example.demo.dto.CreateStudent;
import com.example.demo.entities.CompanyEntity;
import com.example.demo.entities.SchoolEntity;
import com.example.demo.entities.StudentEntity;
import com.example.demo.repositories.CompanyRepository;
import com.example.demo.repositories.SchoolRepository;
import com.example.demo.repositories.StudentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SchoolControllerTestTwo {

    @LocalServerPort
    int port;

    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    SchoolRepository schoolRepository;

    List<StudentEntity> studentList;
    StudentEntity Niclas;

    List<SchoolEntity> schoolEntityList;

    @BeforeEach
    void beforeEach() {
        schoolEntityList= schoolRepository.findAll();
        Niclas = new StudentEntity("1", "KungNiclas", "Niclas@ÄrBäst.se", "Stockholm", "java", "EC", null, null);
        studentList = studentRepository.findAll();

    }


//    @Test
//    void Test_Post_Student() {
//        WebClient webClient = WebClient.create("http://localhost:" + port + "/school/createStudentAccount");
//
//        Student piu = new Student("35", "Piu", "Piu@ÄrBäst.se", "Stockholm", "java", "EC", null, null, new ArrayList<>());
//        Student student = webClient.post()
//                .header(MediaType.APPLICATION_JSON_VALUE)
//                .body(Mono.just(piu), StudentEntity.class)
//                .retrieve()
//                .bodyToMono(Student.class)
//                .block();
//
//        assertEquals("Piu", student.getName());
//        assertEquals(piu, student);
//
//        studentRepository.deleteById("35");
//    }

//    @Test
//    void Test_Post_Company() {
//        WebClient webClient = WebClient.create("http://localhost:" + port + "/school/createCompanyAccount");
//
//        Company testCompany = new Company("35", "PiuInc", "Piu@ÄrBäst.se", new ArrayList<>(), "Stockholm", "null");
//        System.out.println( testCompany.getId());
//        Company company = webClient.post()
//                .header(MediaType.APPLICATION_JSON_VALUE)
//                .body(Mono.just(testCompany), CompanyEntity.class)
//                .retrieve()
//                .bodyToMono(Company.class)
//                .block();
//
//        assertEquals("PiuInc", company.getName());
//        assertEquals(testCompany.getName(), company.getName());
////        assertEquals(testCompany.getName(), company.getName());
//
//        companyRepository.deleteById("35");
//    }


    @Test
    void Test_Get_List_Of_Students() {
        WebClient webClient = WebClient.create("http://localhost:" + port + "/school/students");


        List<Student> students = webClient.get()
                .retrieve()
                .bodyToFlux(Student.class)
                .collectList()
                .block();
        List<String> studentEntityName = studentList.stream().map(StudentEntity::getName).collect(Collectors.toList());
        List<String> stringList = students.stream().map(Student::getName).collect(Collectors.toList());

        assertEquals(studentEntityName, stringList);

    }

    @Test
    void Test_Get_List_Of_Companys() {
        WebClient webClient = WebClient.create("http://localhost:" + port + "/school/companies");

        List<Company> companys = webClient.get()
                .retrieve()
                .bodyToFlux(Company.class)
                .collectList()
                .block();


        assertEquals(8, companys.size());

    }

    @Test
    void Test_Get_Students_By_Id() {
        WebClient webClient = WebClient.create("http://localhost:" + port + "/students/getStudentById/" + Niclas.getId());

        String students = webClient.get()
                .retrieve()
                .bodyToMono(Student.class)
                .map(Student::getName)
                .block();

        assertEquals("KungNiclas", students);
    }

    @Test
    void Test_Get_School_Stream() {
        WebClient webClient = WebClient.create("http://localhost:" + port+"/school/");

        Stream<School> schoolStream = webClient.get()
                .retrieve()
                .bodyToFlux(School.class)
                .toStream();

       List<School> schoolList=  schoolStream.collect(Collectors.toList());


       assertEquals(7,schoolList.size());
       assertEquals("EcUtbildning",schoolList.get(0).getName());

    }
}
