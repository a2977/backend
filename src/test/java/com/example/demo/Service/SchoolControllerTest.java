package com.example.demo.Service;

import com.example.demo.domains.Company;
import com.example.demo.domains.Student;
import com.example.demo.entities.CompanyEntity;
import com.example.demo.entities.StudentEntity;
import com.example.demo.repositories.CompanyRepository;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@ActiveProfiles("integrationtest")
public class SchoolControllerTest {


    @LocalServerPort
    int port;
    @Autowired
    CompanyRepository companyRepository;

    CompanyEntity company;

    @BeforeEach
    void beforeEach() throws Exception {
        company = new CompanyEntity("2", "Dennise", "email", null, "Stocholm", "image url");
        companyRepository.save(company);
    }


    @Test
    @Transactional
    public void test_Create_company_account() {
        CompanyEntity companyTest = new CompanyEntity("1", "Madde", "email", null, "Stocholm", "imgae url");
        WebClient webClient = WebClient.create("http://localhost:"+port);

        Company company = webClient.post()
                .uri("school/createCompanyAccount")
                .header(MediaType.APPLICATION_JSON_VALUE)
                .body(Mono.just(companyTest), CompanyEntity.class)
                .retrieve()
                .bodyToMono(Company.class)
                .block();

        assertEquals("Madde", company.getName());

    }
        @Test
        public void Test_create_student () {
            StudentEntity studentEntity = new StudentEntity("65", "Djako", "email", "Vasterås", "Java", "ECkallas", "img", null);
            WebClient webClient = WebClient.create("http://localhost:"+port);
            Student student = webClient.post()
                    .uri("/school/createStudentAccount")
                    .header(MediaType.APPLICATION_JSON_VALUE)
                    .body(Mono.just(studentEntity), StudentEntity.class)
                    .retrieve()
                    .bodyToMono(Student.class)
                    .block();

            assertEquals("Djako", student.getName());

        }

    }

