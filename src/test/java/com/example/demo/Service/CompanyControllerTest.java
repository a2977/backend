package com.example.demo.Service;

import com.example.demo.domains.Company;
import com.example.demo.domains.Student;
import com.example.demo.entities.CompanyEntity;
import com.example.demo.repositories.CompanyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CompanyControllerTest {

    @LocalServerPort
    int port;
    @Autowired
    CompanyRepository companyRepository;


    @Test
    void itShould_Get_Company_ById() {

        WebClient webClient = WebClient.create("http://localhost:" + port + "/company/getById/1");

        String company = webClient.get()
                .retrieve()
                .bodyToMono(Company.class)
                .map(Company::getName)
                .block();
        assertEquals("Volvo", company);
    }
}
//    @Test
//    void itShould_Get_Student_ById() {
//
//        WebClient webClient = WebClient.create("http://localhost:" + port + "/company/student/Micke");
//
//        List<Student> student = webClient.get()
//                .retrieve()
//                .bodyToFlux(Student.class)
//                .collectList()
//                .block();
//
//
//        assertEquals("Micke",student.get(0).getName() );
//    }
//}
