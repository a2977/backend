package com.example.demo.Service;

import com.example.demo.dto.CreateSchool;
import com.example.demo.entities.SchoolEntity;
import com.example.demo.exceptions.SchoolException;
import com.example.demo.repositories.CompanyRepository;
import com.example.demo.repositories.SchoolRepository;
import com.example.demo.repositories.StudentRepository;
import com.example.demo.services.SchoolService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SchoolServiceTest {
    SchoolService schoolService;
    SchoolRepository schoolRepositoryMock;
    CompanyRepository companyRepositoryMock;
    StudentRepository studentRepositoryMock;

    SchoolService schoolServiceMock;

    CreateSchool createSchool;
    SchoolEntity schoolEntityExpected;

    SchoolEntity diako;
    SchoolEntity dan;
    SchoolEntity dennise;

    List<SchoolEntity> entities;

    SchoolEntity expetedSchoolEntityDiako;
    SchoolEntity expetedSchoolEntityDan;
    SchoolEntity expetedSchoolEntityDennise;

//    @BeforeEach
//    void setUp() {
//        schoolRepositoryMock = mock(SchoolRepository.class);
//        companyRepositoryMock = mock(CompanyRepository.class);
//        studentRepositoryMock = mock(StudentRepository.class);
//
//
//        this.schoolService = new SchoolService(schoolRepositoryMock, companyRepositoryMock,studentRepositoryMock);
//        this.schoolServiceMock = mock(SchoolService.class);
//
//        createSchool = new CreateSchool("1", "Diako", "diaoko@email", "Bästerås");
//        schoolEntityExpected = new SchoolEntity("1", "Diako", "diaoko@email", "Bästerås",null);
//
//        entities = Arrays.asList(
//                new SchoolEntity("1", "Diako", "diaoko@email", "Vås",null),
//                new SchoolEntity("2", "Dan", "Dan@email", "Uppsala",null),
//                new SchoolEntity("3", "Dennise", "Den@email", "Örebro",null));
//
//        diako = new SchoolEntity("1", "Diako", "diaoko@email", "Vås",null);
//        dan = new SchoolEntity("2", "Dan", "Dan@email", "Uppsala", null);
//        dennise = new SchoolEntity("3", "Dennise", "Den@email", "Örebro",null);
//
//        expetedSchoolEntityDiako = new SchoolEntity("1", "Diako", "diaoko@email", "Vås",null);
//        expetedSchoolEntityDan = new SchoolEntity("2", "Dan", "Dan@email", "Uppsala",null);
//        expetedSchoolEntityDennise = new SchoolEntity("3", "Dennise", "Den@email", "Örebro",null);
//    }

    @Test
    void itShould_Create_school() {
        //Given
        String expectedName = "Diako";

        //When
        SchoolEntity schoolEntityActual = schoolService.createSchool(createSchool);
        //Then
        assertEquals(expectedName, schoolEntityActual.getName());
        assertEquals(schoolEntityExpected, schoolEntityActual);
    }

    @Test

    void itShould_Return_Stream_Of_SchoolEntity() {
        when(schoolServiceMock.all()).thenReturn(Stream.of(diako, dan, dennise));
        int expectedNrOfSchools = 3;

        List<SchoolEntity> schoolEntityActual = schoolServiceMock.all().collect(Collectors.toList());

        assertEquals(expectedNrOfSchools, schoolEntityActual.size());
        assertEquals(entities, schoolEntityActual);
    }

    @Test
    void itShould_Return_Stream_Of_SchoolEntity_Multiple_Calls() {

        when(schoolServiceMock.all())
                .thenReturn(Stream.of(diako))
                .thenReturn(Stream.of(dan))
                .thenReturn(Stream.of(dennise));


        Stream<SchoolEntity> schoolEntityActual = schoolServiceMock.all();
        Stream<SchoolEntity> schoolEntityActualSecond = schoolServiceMock.all();
        Stream<SchoolEntity> schoolEntityActualThired = schoolServiceMock.all();

        //Then
        assertAll(
                () -> assertEquals(expetedSchoolEntityDiako, schoolEntityActual.findFirst().get()),
                () -> assertEquals(expetedSchoolEntityDan, schoolEntityActualSecond.findFirst().get()),
                () -> assertEquals(expetedSchoolEntityDennise, schoolEntityActualThired.findFirst().get())
        );

    }


    @Test
    void itShould_Return_Singel_SchoolEntity_FromSchoolRepository_ById() throws SchoolException {
        when(schoolRepositoryMock.getById("1")).thenReturn(diako);

        SchoolEntity expectedSchoolActual = schoolRepositoryMock.getById("1");
        System.out.println(expectedSchoolActual);

        assertEquals(expetedSchoolEntityDiako, expectedSchoolActual);
    }

    @Test
    void itShould_Return_Singel_SchoolEntity_FromSchoolService_ById() throws SchoolException {
        when(schoolServiceMock.getById("1")).thenReturn(diako);

        SchoolEntity expectedSchoolActual = schoolServiceMock.getById("1");
        assertEquals(expetedSchoolEntityDiako, expectedSchoolActual);
    }


}

