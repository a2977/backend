package com.example.demo.controllers;

import com.example.demo.domains.Company;
import com.example.demo.domains.Student;
import com.example.demo.dto.CreateCompany;
import com.example.demo.dto.CreateStudent;


import com.example.demo.entities.CompanyEntity;
import com.example.demo.entities.StudentEntity;


import static org.hamcrest.Matchers.*;
import com.example.demo.repositories.CompanyRepository;
import com.example.demo.repositories.StudentRepository;
import com.example.demo.services.StudentService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import javax.transaction.Transactional;

@SpringBootTest
@ActiveProfiles("integrationtest")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@AutoConfigureMockMvc

class StudentControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    SchoolController schoolController;
    @Autowired
    StudentController studentController;
    @Autowired
    StudentService studentService;
    @Autowired
    StudentRepository studentRepository;

    @Autowired
    CompanyRepository companyRepository;


    private Student student;
    private Company company;
    private Company company2;
    private CompanyEntity companyEntity;
    private StudentEntity studentEntity;

    @BeforeAll
    @DisplayName("Initialization")
    public void init() {
        student = schoolController.createStudent(new CreateStudent( "Kalle","kalle@email.com", "Karlstad", "Java", "EC-utb", null,null));

        company = schoolController.createCompany(new CreateCompany("jakob och Teo godisbutik", "jakTeo@work.se","java", "sthlm"));
        company2 = schoolController.createCompany(new CreateCompany("adam", "adam@work.se", "java","sthlm"));

        studentEntity = new StudentEntity("1", "Jimme", "Jimmie3an@businesses.com", "göteborg", "arbetslös", "ingen skola", null,null);
        companyEntity = new CompanyEntity("2", "Volvo", "volvo@workk.se","städare", "Volvo City", null);

        studentRepository.save(studentEntity);
        companyRepository.save(companyEntity);
    }


//    @Test
//    @DisplayName("Student creation")
//    void createStudent() {
//        assertNotNull(student.getId());
//        assertEquals("1", student.getId(), "get id failed" );
//        assertEquals("Kalle", student.getName(), "get name failed");
//        assertEquals("kalle@email.com", student.getEmail(), "get email failed");
//        assertEquals("Karlstad", student.getCity(), () -> "get city failed");
//        assertEquals("Java", student.getCourse(), () -> "get course failed");
//        assertEquals("EC-utb", student.getSchool(), () -> "get school failed");
//    }

    @Test
    @Transactional
    void getStudentById() throws Exception {
        gettingStudents();
    }

    @Test
    @Transactional
    void addToFavorites() throws Exception {
        addingToFavorites();
    }

    @Test
    @Transactional
    void favoriteCompanies() throws Exception {

        listFavorites();
    }

    @ParameterizedTest
    @ValueSource(strings ={"ada", "adam", "ad"})
    @Transactional
    void searchCompanies(String value) throws Exception {
        companySearch(value);
    }




    @Test
    void uploadFile() {
    }


    private  ResultActions gettingStudents() throws Exception {
        String url =   "http://localhost:8080/students/getStudentById/" + studentEntity.getId();
        ResultActions resultActions = mockMvc.perform(get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(containsString(studentEntity.getName().toString())));
        return resultActions;
    }

    public ResultActions addingToFavorites() throws Exception {
        String url = "http://localhost:8080/students/saveCompanyToFavorites/" + studentEntity.getId() + "/" + companyEntity.getId();
        ResultActions resultActions = mockMvc.perform(get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(containsString(companyEntity.getName().toString())));
        return resultActions;
    }

    //WHEN
    public ResultActions listFavorites() throws Exception {
        String favoriteContent= "[]";

        String url = "http://localhost:8080/students/favoriteCompanies/" + studentEntity.getId();
        ResultActions resultActions = mockMvc.perform(get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(containsString(favoriteContent)));

        return resultActions;
    }

    public ResultActions companySearch(String searchVal) throws Exception {
        Company company2 = schoolController.createCompany(new CreateCompany("adam", "adam@work.se", "java","sthlm"));
        List<Company> searchedCompany = studentController.searchCompanies(searchVal);
        String url = "http://localhost:8080/students/companies/" + searchVal;
        ResultActions resultActions = mockMvc.perform(get(url));
        mockMvc.perform(get("http://localhost:8080/students/companies/"+ searchVal))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(searchVal)));
        return resultActions;

    }

}
