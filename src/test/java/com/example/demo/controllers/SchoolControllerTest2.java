package com.example.demo.controllers;


import com.example.demo.domains.Company;
import com.example.demo.domains.School;
import com.example.demo.domains.Student;
import com.example.demo.dto.CreateCompany;
import com.example.demo.dto.CreateSchool;
import com.example.demo.dto.CreateStudent;
import com.example.demo.entities.CompanyEntity;
import com.example.demo.entities.StudentEntity;
import com.example.demo.repositories.CompanyRepository;
import com.example.demo.repositories.SchoolRepository;
import com.example.demo.repositories.StudentRepository;
import com.example.demo.services.CompanyService;
import com.example.demo.services.SchoolService;
import com.example.demo.services.StudentService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.demo.domains.Company;
import com.example.demo.dto.CreateCompany;
import com.example.demo.entities.CompanyEntity;
import com.example.demo.services.CompanyService;
import com.example.demo.services.SchoolService;
import com.example.demo.services.StudentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@SpringBootTest
@ActiveProfiles("integrationtest")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DisplayName("SchoolController OG")
@AutoConfigureMockMvc
public class SchoolControllerTest2 {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    StudentRepository studentRepository;
    @Autowired
    SchoolRepository schoolRepository;
    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    StudentService studentService;
    @Autowired
    CompanyService companyService;
    @Autowired
    SchoolService schoolService;

    @Autowired
    SchoolController schoolController;
    @Autowired
    AdminController adminController;


    private Student student;
    private School school;
    private Company company;

    private CompanyEntity companyEntity;
    private StudentEntity studentEntity;


//    @BeforeAll
//    @DisplayName("init")
//    void setUp() {
//        student = schoolController.createStudent(new CreateStudent( "Kalle","kalle@email.com", "Karlstad", "Java", "EC-utb", null,null));
//
//        company = schoolController.createCompany(new CreateCompany("jakob och Teo godisbutik", "jakTeo@work.se", "java","sthlm"));
//        school = adminController.createSchool(new CreateSchool("1", "astronauts", "as@work.se", "lost city"));
//
//        studentEntity = new StudentEntity("1", "Jimme", "Jimmie3an@businesses.com", "goteborg", "arbetslos", "ingen skola", null,null);
//        companyEntity = new CompanyEntity("2", "Volvo", "volvo@workk.se", "java","Volvo City", null);
//
//        studentRepository.save(studentEntity);
//        companyRepository.save(companyEntity);
//    }


    @Test
    @Transactional
    @DisplayName("Company creation")

    void test_createCompany()  {

        assertNotNull(company.getId());
        //assertEquals("1", company.getId(), "get id failed" ); funkar inte nu när det är ett uuid istället för ett hårdkodat id
        assertEquals("jakob och Teo godisbutik", company.getName(), "get name failed");
        assertEquals("jakTeo@work.se", company.getEmail(), "get email failed");
        assertEquals("sthlm", company.getCity(), () -> "get city failed");

    }

    @Test
    @DisplayName("Student creation")
    void test_createStudent() {

        assertNotNull(student.getId());
       // assertEquals("1", student.getId(), () -> "get id failed" ); funkar inte nu när det är ett uuid istället för ett hårdkodat id
        assertEquals("Kalle", student.getName(), () -> "get name failed");
        assertEquals("kalle@email.com", student.getEmail(), () -> "get email failed");
        assertEquals("Karlstad", student.getCity(), () -> "get city failed");
        assertEquals("Java", student.getCourse(), () -> "get course failed");
        assertEquals("EC-utb", student.getSchool(), () -> "get school failed");
    }


    @Test
    @Transactional
    @DisplayName("Getting Companies")
    void test_getCompanies() throws Exception {

        mockMvc.perform(
                        get("http://localhost:8080/school/companies"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(containsString(companyEntity.getName().toString())))
        ;

    }

    @Test
    @Transactional
    @DisplayName("Getting Student")
    void test_getStudents() throws Exception {

        mockMvc.perform(
                        get("http://localhost:8080/school/students"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(containsString(studentEntity.getName().toString())));

    }

    @Test
    @Transactional
    @DisplayName("Getting Schools")
    void test_getSchools() throws Exception {

        mockMvc.perform(
                        get("http://localhost:8080/school/"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(containsString(school.getName())));
    }

    @Test
    @DisplayName("Getting School By Id")
    void test_getSchoolById() throws Exception {

        mockMvc.perform(
                        get("http://localhost:8080/school/getById/" + school.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(containsString(school.getId())));
    }
}

