package com.example.demo.controllers;

import com.example.demo.domains.School;
import com.example.demo.domains.Student;
import com.example.demo.dto.CreateSchool;
import com.example.demo.dto.CreateStudent;
import com.example.demo.entities.SchoolEntity;
import com.example.demo.entities.StudentEntity;
import com.example.demo.exceptions.ResponseFile.ResponseMessage;
import com.example.demo.exceptions.StudentException;
import com.example.demo.repositories.SchoolRepository;
import com.example.demo.repositories.StudentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

@SpringBootTest
@ActiveProfiles("integrationtest")
class FileControllerTest {

    @Autowired
    FileController fileController;
    @Autowired
    SchoolController schoolController;
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    AdminController adminController;
    @Autowired
    SchoolRepository schoolRepository;

    @Test
    void name() {
        MultipartFile file = mock(MultipartFile.class);
        ResponseEntity<ResponseMessage> responseMessageResponseEntity = fileController.uploadFile(file);

        assertEquals(200, responseMessageResponseEntity.getStatusCodeValue());
    }

    @Test
    @Transactional
    void test_upload_student_image_file() throws StudentException {
        MultipartFile file = mock(MultipartFile.class);

        Student student = schoolController.createStudent(new CreateStudent(
                "Kalle",
                "kalle@email.com",
                "Karlstad",
                "Java",
                "EC-utb",
                null,
                null));

        ResponseEntity<ResponseMessage> responseMessageResponseEntity = fileController.uploadStudentImageFile(student.getId(), file);
        System.out.println(file);

        assertEquals(200, responseMessageResponseEntity.getStatusCodeValue());
        StudentEntity student1 = studentRepository.getById(student.getId());
        assertNotNull(student1.getImageId());
    }
}
