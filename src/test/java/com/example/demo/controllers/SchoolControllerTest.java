package com.example.demo.controllers;


import com.example.demo.domains.Company;
import com.example.demo.domains.School;
import com.example.demo.domains.Student;
import com.example.demo.dto.CreateCompany;
import com.example.demo.dto.CreateSchool;
import com.example.demo.dto.CreateStudent;
import com.example.demo.repositories.CompanyRepository;
import com.example.demo.repositories.SchoolRepository;
import com.example.demo.repositories.StudentRepository;
import com.example.demo.services.CompanyService;
import com.example.demo.services.SchoolService;
import com.example.demo.services.StudentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ActiveProfiles("integrationtest")
class SchoolControllerTest {


    @Autowired
    SchoolController schoolController;
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    SchoolRepository schoolRepository;
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    StudentService studentService;
    @Autowired
    CompanyService companyService;
    @Autowired
    SchoolService schoolService;
    @Autowired
    AdminController adminController;


    @BeforeEach
    void setUp() {
        companyRepository.deleteAll();
        studentRepository.deleteAll();
        schoolRepository.deleteAll();
    }

    @Test
    void test_create_company_success() {
        //Given
        Company company = schoolController.createCompany(new CreateCompany("Google", "google@google.com", "java", "Stockholm"));
        String id = company.getId();

        //Then
        assertNotNull(company.getId());
        assertEquals(id, company.getId());
        assertEquals("Google", company.getName());
        assertEquals("google@google.com", company.getEmail());
        assertEquals("Stockholm", company.getCity());
    }

//    @Test
//    void test_create_student_success() {
//        //Given
//        Student student = schoolController.createStudent(new CreateStudent("Kalle",
//                "kalle@email.com", "Karlstad", "Java", "EC-utb",
//                null,null));
//        //Then
//        assertNotNull(student.getId());
//        assertEquals("1", student.getId());
//        assertEquals("Kalle", student.getName());
//        assertEquals("kalle@email.com", student.getEmail());
//        assertEquals("Karlstad", student.getCity());
//        assertEquals("Java", student.getCourse());
//        assertEquals("EC-utb", student.getSchool());
//    }


//    @Test
//    void test_get_companies_success() {
//        //Given
//        schoolController.createCompany(new CreateCompany("Google", "google@google.com", "java","Stockholm"));
//        schoolController.createCompany(new CreateCompany("Volvo", "volvo@google.com", "java", "Göteborg"));
//        schoolController.createCompany(new CreateCompany("Saab", "saab@google.com",  "java","Uppsala"));
//
//        //When
//        List<Company> companies = schoolController.getCompanies();
//
//        //Then
//        assertEquals(3, companies.size());
//        assertNotNull(companies.get(1).getId());
//        assertEquals("Volvo", companies.get(1).getName());
//        assertEquals("Stockholm", companies.get(0).getCity());
//        assertEquals("saab@google.com", companies.get(2).getEmail());
//    }

//    @Test
//    @Transactional
//    void test_get_students_success() {
//        //Given
//        schoolController.createStudent(new CreateStudent("Mats","mats@email","Uppsala","Java","Ecutbildning","image url",null));
//        schoolController.createStudent(new CreateStudent("Dan","dan@email","Karlsta","JavaScript","Nacka","image url",null));
//        schoolController.createStudent(new CreateStudent("Tom","tom@email","Örebro","Css","Nti","image url",null));
//
//        //When
//        List<Student> students = schoolController.getStudents();
//        System.out.println(students);
//
//        //Then
//        assertThat(students).element(0).isEqualTo(students.get(0));
//        assertEquals(3, students.size());
//        assertEquals("1", students.get(0).getId());
//        assertEquals("2", students.get(1).getId());
//        assertEquals("3", students.get(2).getId());
//    }

//    @Test
//    void test_get_schools_success() {
//        //GIVEN
//        String id = UUID.randomUUID().toString();
//        School school = adminController.createSchool(new CreateSchool(id,"EC","EC@Gmail.com","Stockholm"));
//
//        //THEN
//        assertNotNull(school.getId());
//        assertEquals(id, school.getId());
//        assertEquals("EC", school.getName());
//        assertEquals("EC@Gmail.com", school.getEmail());
//        assertEquals("Stockholm", school.getCity());
//    }

//    @Test
//    void test_getSchool_by_id_success() {
//        //GIVEN
//        String id = UUID.randomUUID().toString();
//        adminController.createSchool(new CreateSchool(id,"EC","EC@Gmail.com","Stockholm"));
//
//        //WHEN
//        List<School> schools = schoolController.getSchools().collect(Collectors.toList());
//
//        //THEN
//        assertEquals(1, schools.size());
//        assertEquals(id, schools.get(0).getId());
//        assertEquals("EC", schools.get(0).getName());
//        assertEquals("EC@Gmail.com", schools.get(0).getEmail());
//    }

}
