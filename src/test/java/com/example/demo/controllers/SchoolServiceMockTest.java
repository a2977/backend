package com.example.demo.controllers;

import com.example.demo.domains.School;
import com.example.demo.dto.CreateSchool;
import com.example.demo.entities.SchoolEntity;
import com.example.demo.entities.StudentEntity;
import com.example.demo.repositories.SchoolRepository;
import com.example.demo.services.SchoolService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("integrationtest")
public class SchoolServiceMockTest {

    @Autowired
    SchoolService schoolService;
    @MockBean
    SchoolRepository schoolRepository;

    @Test
    void school_all() {
        when(schoolRepository.findAll()).thenReturn(Collections.emptyList());
        assertTrue(schoolService.findAll().isEmpty());
    }

//    @Test
//    void test_Create_School() {
//        //Given
//        when(schoolRepository.findAll()).thenReturn(Collections.emptyList());
//        when(schoolRepository.save(any())).then(invocation -> invocation.getArguments()[0]);
//
//        //When
//        SchoolEntity school = schoolService.createSchool(new CreateSchool("1", "EC", "EC@gmail.se", "Stockholm"));
//
//        //Then
//        verify(schoolRepository).save(school);
//        assertEquals("1", school.getId());
//        assertEquals("EC",school.getName());
//        assertEquals("Stockholm",school.getCity());
//    }
}
