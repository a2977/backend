package com.example.demo.controllers;

import com.example.demo.domains.Company;
import com.example.demo.domains.Student;
import com.example.demo.dto.CreateCompany;
import com.example.demo.dto.CreateStudent;
import com.example.demo.entities.CompanyEntity;
import com.example.demo.entities.StudentEntity;
import com.example.demo.repositories.CompanyRepository;
import com.example.demo.repositories.SchoolRepository;
import com.example.demo.services.CompanyService;
import com.example.demo.services.SchoolService;
import com.example.demo.services.StudentService;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@SpringBootTest
@ActiveProfiles("integrationtest")
class SchoolTestMockController {

    @Autowired
    SchoolController schoolController;
    @MockBean
    StudentService studentService;
    @MockBean
    CompanyService companyService;
    @MockBean
    SchoolService schoolService;




    @Test
    void test_create_company_success() {
        //Given
        when(companyService.createCompany(any(CreateCompany.class))).thenReturn(new CompanyEntity("1", "Google", "google@google.com", "java","Stockholm",null));

        //When
        Company company = schoolController.createCompany(new CreateCompany("Google", "google@google.com", "java","Stockholm"));

        //Then
        assertNotNull(company.getId());
        assertEquals("1", company.getId());
        assertEquals("Google", company.getName());
        assertEquals("google@google.com", company.getEmail());
        assertEquals("Stockholm", company.getCity());
    }

    @Test
    void test_create_student_success(){
        //GIVEN
        when(studentService.createStudent(any(CreateStudent.class))).thenReturn(new StudentEntity("1","Niclas","Google@gmail.se","Stockholm","Java","EC",null,null));
        //WHEN
        Student student = schoolController.createStudent(new CreateStudent("Niclas","Google@gmail.se","Stockholm","Java","EC",null,null));
        //THEN
        assertNotNull(student.getId());
        assertEquals("1", student.getId());
        assertEquals("Niclas", student.getName());
        assertEquals("Google@gmail.se", student.getEmail());
        assertEquals("Stockholm", student.getCity());
        assertEquals("Java", student.getCourse());
        assertEquals("EC", student.getSchool());
        assertNull(student.getImageId());
        assertNull(student.getCv());
    }

//    @Test
//    void test_get_companies(){
//        //GIVEN
//        CompanyEntity comp1 = new CompanyEntity("1","Niclas","niclas@gmail.com", "java","stockholm",null);
//        CompanyEntity comp2 = new CompanyEntity("2","Oscar","niclas@gmail.com", "java","stockholm",null);
//        when(companyService.getAllCompanies()).thenReturn(Stream.of(comp1,comp2));
//
//        //WHEN
//        List<Company> company = schoolController.getCompanies();
//
//        //THEN
//        assertEquals(2, company.size());
//        assertNotNull(company.get(1).getId());
//        assertEquals("Oscar", company.get(1).getName());
//        assertEquals("stockholm", company.get(0).getCity());
//        assertEquals("niclas@gmail.com", company.get(1).getEmail());
//    }
//
//    @Test
//    void test_get_students(){//TODO: Change Email?
//        //GIVEN
//        StudentEntity student1 = new StudentEntity("1","Christian","Christan@GeOssGodKäntFan.com","Stockholm","Java","EC",null,null);
//        StudentEntity student2 = new StudentEntity("2","Christian","Christan@GeOssGodKänt_Kommigen.com","Stockholm","PHP","EC",null,null);
//        when(studentService.allStudents()).thenReturn(Stream.of(student1,student2));
//
//        //WHEN
//        List<Student> studentList = schoolController.getStudents();
//
//        //THEN
//        assertEquals(2, studentList.size());
//        assertNotNull(studentList.get(1).getId());
//        assertEquals("Christian", studentList.get(1).getName());
//        assertEquals("Stockholm", studentList.get(0).getCity());
//        assertEquals("Christan@GeOssGodKäntFan.com", studentList.get(0).getEmail());
//    }
}
