package com.example.demo.controllers;

import com.example.demo.domains.Company;
import com.example.demo.domains.School;
import com.example.demo.domains.Student;
import com.example.demo.dto.CreateCompany;
import com.example.demo.dto.CreateStudent;
import com.example.demo.dto.FilterCriteria;
import com.example.demo.entities.StudentEntity;
import com.example.demo.entityToDto.CompanyEntityToDto;
import com.example.demo.entityToDto.SchoolEntityToDto;
import com.example.demo.entityToDto.StudentEntityToDto;
import com.example.demo.exceptions.CompanyException;
import com.example.demo.exceptions.SchoolException;
import com.example.demo.exceptions.StudentException;
import com.example.demo.services.CompanyService;
import com.example.demo.services.SchoolService;
import com.example.demo.services.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/school")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*", exposedHeaders = "X-Total-Count")
public class SchoolController implements CompanyEntityToDto, SchoolEntityToDto, StudentEntityToDto {

    SchoolService schoolService;
    CompanyService companyService;
    StudentService studentService;

    @PostMapping("/createCompanyAccount")
    public Company createCompany(@RequestBody CreateCompany createCompany) {
        return companyEntityToDto(companyService.createCompany(createCompany));
    }

    @GetMapping("/companies")
    public List<Company> getCompanies(HttpServletResponse response) {
        response.addHeader("X-Total-Count",studentService.allStudents().count() + "");
        return companyService.getAllCompanies().map(this::companyEntityToDto).collect(Collectors.toList());
    }

    @GetMapping("/companies/{id}")
    public Company getCompaniesByID(@PathVariable String id) throws CompanyException {
        return companyEntityToDto(companyService.getById(id));
    }

    @GetMapping("/schools")
    public Stream<School> getSchools(HttpServletResponse response) {
        response.addHeader("X-Total-Count",studentService.allStudents().count() + "");
        return schoolService.all().map(this::schoolEntityToDto);
    }

    @GetMapping("/schools/{id}")
    public School getSchoolById(@PathVariable String id) throws SchoolException {
        return schoolEntityToDto(schoolService.getById(id));
    }

    @PostMapping("/createStudentAccount")
    public Student createStudent(@RequestBody CreateStudent createStudent) {
        return studentEntityToDto(studentService.createStudent(createStudent));
    }

    @GetMapping("/students/{id}")
    public Student getStudentById(@PathVariable String id) throws StudentException {
        return studentEntityToDto(studentService.getById(id));
    }

    @GetMapping("/students")
    public List<Student> getStudents(HttpServletResponse response) {
        response.addHeader("X-Total-Count",studentService.allStudents().count() + "");
        return studentService.allStudents().map(this::studentEntityToDto).collect(Collectors.toList());
    }

    @PostMapping("/students/filter")
    public List<StudentEntity> getStudentsFiltered(@RequestBody FilterCriteria filteredCriteria) {
        return studentService.allStudents()
                .filter(studentEntity -> filteredCriteria.getCities().contains(studentEntity.getCity()))
                .filter(studentEntity -> filteredCriteria.getCourses().contains(studentEntity.getCourse()))
//                .filter(studentEntity -> filteredCriteria.getCompanies().contains(studentEntity.getCompanies()))
                .collect(Collectors.toList());
    }

    @GetMapping("/company/cities")
    public Stream<String> getCities() {
        return schoolService.getCities();
    }


    @GetMapping("/student/professions")
    public Stream<String> getProfessions() {
        return schoolService.getProfessions();
    }
}
