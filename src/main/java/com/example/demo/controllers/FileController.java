package com.example.demo.controllers;

import com.example.demo.domains.FileDB;
import com.example.demo.exceptions.CompanyException;
import com.example.demo.exceptions.ResponseFile.ResponseFile;
import com.example.demo.exceptions.ResponseFile.ResponseMessage;
import com.example.demo.exceptions.SchoolException;
import com.example.demo.exceptions.StudentException;
import com.example.demo.repositories.StudentRepository;
import com.example.demo.services.FileStorageService;
import com.example.demo.services.SchoolService;
import com.example.demo.services.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


@RestController
@RequestMapping("")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class FileController {

    StudentService studentService;
    FileStorageService fileStorageService;
    SchoolService schoolService;
    StudentRepository studentRepository;

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file) {
        return fileStorageService.getUploadFile(file);
    }

    @GetMapping("/files/{id}")
    public ResponseEntity<byte[]> getFile(@PathVariable String id) {
        FileDB fileDB = fileStorageService.getFile(id);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileDB.getName() + "\"")
                .body(fileDB.getData());
    }


    @GetMapping("/files")
    public ResponseEntity<List<ResponseFile>> getListFiles() {
        List<ResponseFile> files = fileStorageService.getFileList();
        return ResponseEntity.status(HttpStatus.OK).body(files);
    }

    @RequestMapping(value = "/uploadStudentImageFile/{studentId}", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public ResponseEntity<ResponseMessage> uploadStudentImageFile(@PathVariable String studentId, MultipartFile file) throws StudentException {
        try {
            fileStorageService.uploadImageToStudent(studentId, file);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage("Uploaded the file successfully: " + file.getOriginalFilename()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage("Could not upload the file: " + file.getOriginalFilename() + "!"));
        }
    }

    @RequestMapping(value = "/uploadCompanyImageFile/{companyId}", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public ResponseEntity<ResponseMessage> uploadCompanyImageFile(@PathVariable String companyId, MultipartFile file) throws CompanyException {
        try {
            fileStorageService.uploadImageToCompany(companyId, file);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage("Uploaded the file successfully: " + file.getOriginalFilename()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage("Could not upload the file: " + file.getOriginalFilename() + "!"));
        }
    }

    @RequestMapping(value = "/uploadStudentCV/{studentId}", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public ResponseEntity<ResponseMessage> uploadStudentCV(@PathVariable String studentId, MultipartFile file) throws StudentException {
        try {
            fileStorageService.uploadStudentCV(studentId, file);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage("Uploaded the file successfully: " + file.getOriginalFilename()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage("Could not upload the file: " + file.getOriginalFilename() + "!"));
        }
    }

    @RequestMapping(value = "/uploadDocumentsToSchool/{schoolId}", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public ResponseEntity<ResponseMessage> uploadDocumentsToSchool(@PathVariable String schoolId, MultipartFile file) throws SchoolException {
        try {
            fileStorageService.uploadDocumentsToSchool(schoolId, file);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage("Uploaded the file successfully: " + file.getOriginalFilename()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage("Could not upload the file: " + file.getOriginalFilename() + "!"));
        }
    }
}
