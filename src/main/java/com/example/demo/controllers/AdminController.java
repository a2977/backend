package com.example.demo.controllers;

import com.example.demo.domains.Company;
import com.example.demo.domains.School;
import com.example.demo.domains.Student;
import com.example.demo.dto.CreateCompany;
import com.example.demo.dto.CreateSchool;
import com.example.demo.dto.CreateStudent;
import com.example.demo.entityToDto.CompanyEntityToDto;
import com.example.demo.entityToDto.SchoolEntityToDto;
import com.example.demo.entityToDto.StudentEntityToDto;
import com.example.demo.exceptions.CompanyException;
import com.example.demo.exceptions.SchoolException;
import com.example.demo.exceptions.StudentException;
import com.example.demo.services.CompanyService;
import com.example.demo.services.SchoolService;
import com.example.demo.services.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/admin")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*", exposedHeaders = "X-Total-Count")
public class AdminController implements SchoolEntityToDto, CompanyEntityToDto, StudentEntityToDto {
    SchoolService schoolService;
    CompanyService companyService;
    StudentService studentService;

    @GetMapping("/companies")
    public List<Company> getCompanies(HttpServletResponse response) {
        response.addHeader("X-Total-Count",studentService.allStudents().count() + "");
        return companyService.getAllCompanies().map(this::companyEntityToDto).collect(Collectors.toList());
    }

    @GetMapping("/companies/{id}")
    public Company getCompaniesByID(@PathVariable String id) throws CompanyException {
        return companyEntityToDto(companyService.getById(id));
    }

    @PostMapping("/companies")
    public Company createAdminCompany(@RequestBody CreateCompany createCompany) {
        return companyEntityToDto(companyService.createCompany(createCompany));
    }

    @PutMapping("/companies/{id}")
    public Company updateCompany(@PathVariable String id,  @RequestBody Map<String,Object> newCompanyInfo) throws CompanyException {
        companyService.companyUpdate(id, newCompanyInfo);
        return companyEntityToDto(companyService.getById(id));
    }

    @DeleteMapping("/companies/{id}")
    public void deleteCompanyById(@PathVariable String id) throws CompanyException {
        companyService.deleteCompany(id);
    }

    @GetMapping("/schools")
    public Stream<School> getSchools(HttpServletResponse response) {
        response.addHeader("X-Total-Count",studentService.allStudents().count() + "");
        return schoolService.all().map(this::schoolEntityToDto);
    }

    @GetMapping("/schools/{id}")
    public School getSchoolById(@PathVariable String id) throws SchoolException {
        return schoolEntityToDto(schoolService.getById(id));
    }

    @PostMapping("/schools")
    public School createAdminSchool(@RequestBody CreateSchool createSchool) {
        return schoolEntityToDto(schoolService.createSchool(createSchool));
    }

    @PutMapping("/schools/{id}")
    public School updateSchool(@PathVariable String id,  @RequestBody Map<String,Object> newSchoolInfo) throws SchoolException {
        schoolService.schoolUpdate(id, newSchoolInfo);
        return schoolEntityToDto(schoolService.getById(id));
    }

    @DeleteMapping("/schools/{id}")
    public String deleteSchoolById(@PathVariable String id) throws SchoolException {
        schoolService.deleteSchool(id);
        return "School successfully deleted";
    }

    @GetMapping("/students")
    public List<Student> getStudents(HttpServletResponse response) {
        response.addHeader("X-Total-Count",studentService.allStudents().count() + "");
        return studentService.allStudents().map(this::studentEntityToDto).collect(Collectors.toList());
    }

    @GetMapping("/students/{id}")
    public Student getStudentById(@PathVariable String id) throws StudentException {
        return studentEntityToDto(studentService.getById(id));
    }

    @PostMapping("/students")
    public Student createAdminStudent(@RequestBody CreateStudent createStudent) {
        return studentEntityToDto(studentService.createStudent(createStudent));
    }

    @PutMapping("/students/{id}")
    public Student updateStudent(@PathVariable String id, @RequestBody Map<String,Object> newStudentInfo) throws StudentException {
        studentService.studentUpdate(id, newStudentInfo);
        return studentEntityToDto(studentService.getById(id));
    }

    @DeleteMapping("/students/{id}")
    public String deleteStudentById(@PathVariable String id) throws StudentException {
        studentService.deleteStudent(id);
        return "student successfully deleted";
    }
}
