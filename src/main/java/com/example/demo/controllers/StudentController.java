package com.example.demo.controllers;

import com.example.demo.domains.Company;
import com.example.demo.entityToDto.CompanyEntityToDto;
import com.example.demo.entityToDto.StudentCompanyEntityToDto;
import com.example.demo.entityToDto.StudentEntityToDto;
import com.example.demo.exceptions.CompanyException;
import com.example.demo.domains.Student;
import com.example.demo.exceptions.StudentException;
import com.example.demo.services.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/students")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class StudentController implements CompanyEntityToDto,StudentEntityToDto,StudentCompanyEntityToDto {
    StudentService studentService;

    @GetMapping("/getStudentById/{id}")
    public Student getStudentById(@PathVariable String id) throws StudentException {
        return studentEntityToDto(studentService.getById(id));
    }

    @Transactional
    @GetMapping("/companies/{searchValue}")
    public List<Company> searchCompanies(@PathVariable String searchValue){
        return studentService.searchCompanies(searchValue).map(this::companyEntityToDto).collect(Collectors.toList());
    }

    @GetMapping("/saveCompanyToFavorites/{studentId}/{companyId}")
    public Student addToFavorites(@PathVariable String studentId, @PathVariable String companyId) throws StudentException, CompanyException {
        return studentEntityToDto(studentService.addToFavorites(studentId, companyId));
    }

    @GetMapping("/favoriteCompanies/{studentId}")
    public Student getStudentsFavorites(@PathVariable String studentId) throws StudentException{
        return studentEntityToDto(studentService.getStudentsFavorites(studentId));
    }

}
