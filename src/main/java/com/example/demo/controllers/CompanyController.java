package com.example.demo.controllers;

import com.example.demo.domains.Company;
import com.example.demo.domains.Student;
import com.example.demo.entities.StudentEntity;
import com.example.demo.entityToDto.CompanyEntityToDto;
import com.example.demo.entityToDto.StudentEntityToDto;
import com.example.demo.exceptions.CompanyException;
import com.example.demo.exceptions.StudentException;
import com.example.demo.services.CompanyService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/company")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CompanyController implements StudentEntityToDto, CompanyEntityToDto {
    CompanyService companyService;

    @GetMapping("/all")
    public List<Company> getCompanies() {
        return companyService.getAllCompanies().map(this::companyEntityToDto).collect(Collectors.toList());
    }

    @GetMapping("/getById/{id}")
    public Company getCompanyById(@PathVariable String id) throws CompanyException {
        return companyService.getCompanyById(id).map(this::companyEntityToDto)
                .orElseThrow(() -> new CompanyException("can't find company with id: " + id));
    }

    @Transactional
    @GetMapping("/students/{searchValue}")
    public List<Student> searchStudents(@PathVariable String searchValue) {
        return companyService.searchStudents(searchValue).map(this::studentEntityToDto).collect(Collectors.toList());
    }

    @GetMapping("/saveStudentToFavorites/{companyId}/{studentId}")
    public Company addToFavorites(@PathVariable String studentId, @PathVariable String companyId) throws StudentException, CompanyException {
        return companyEntityToDto(companyService.addToFavorites(companyId, studentId));
    }

    @GetMapping("/favoriteStudents/{companyId}")
    public List<StudentEntity> getCompaniesFavorites(@PathVariable String companyId) throws CompanyException {
        return companyService.getCompaniesFavorites(companyId).getFavoriteStudents();
    }
//    @GetMapping("/student/{keyVal}")
//    public List<Student> getStudentByName(@PathVariable String keyVal) throws StudentException {
//        return companyService.getStudentByValue(keyVal).map(this::StudentEntityToDto).collect(Collectors.toList());
//    }

}



