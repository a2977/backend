package com.example.demo.services;

import com.example.demo.domains.FileDB;
import com.example.demo.domains.School;
import com.example.demo.entities.CompanyEntity;
import com.example.demo.entities.SchoolEntity;
import com.example.demo.entities.StudentEntity;
import com.example.demo.exceptions.CompanyException;
import com.example.demo.exceptions.ResponseFile.ResponseFile;
import com.example.demo.exceptions.ResponseFile.ResponseMessage;
import com.example.demo.exceptions.SchoolException;
import com.example.demo.exceptions.StudentException;
import com.example.demo.repositories.CompanyRepository;
import com.example.demo.repositories.FileDBRepository;
import com.example.demo.repositories.SchoolRepository;
import com.example.demo.repositories.StudentRepository;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class FileStorageService {

    StudentRepository studentRepository;
    FileDBRepository fileDBRepository;
    CompanyRepository companyRepository;
    SchoolRepository schoolRepository;

    public FileDB store(MultipartFile file) throws IOException {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        FileDB FileDB = new FileDB(fileName, file.getContentType(), file.getBytes());

        return fileDBRepository.save(FileDB);
    }

    public FileDB getFile(String id) {
        return fileDBRepository.findById(id).get();
    }

    public Stream<FileDB> getAllFiles() {
        return fileDBRepository.findAll().stream();
    }

    public void uploadImageToStudent(String studentId, MultipartFile file) throws StudentException, IOException {
        StudentEntity student = studentRepository.getById(studentId);

        FileDB fileDB = store(file);
        student.setImageId(fileDB.getId());
        studentRepository.save(student);
    }

    public void uploadImageToCompany(String companyId, MultipartFile file) throws CompanyException, IOException {
        CompanyEntity company = companyRepository.getById(companyId);
        FileDB fileDB = store(file);
        company.setImageId(fileDB.getId());
        companyRepository.save(company);
    }

    public void uploadStudentCV(String studentId, MultipartFile file) throws StudentException, IOException {
        StudentEntity student = studentRepository.getById(studentId);
        FileDB fileDB = store(file);
        student.setCv(fileDB.getId());
        studentRepository.save(student);
    }

    public List<ResponseFile> getFileList() {
        return getAllFiles().map(dbFile -> {
            String fileDownloadUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/files/")
                    .path(dbFile.getId())
                    .toUriString();

            return new ResponseFile(
                    dbFile.getName(),
                    fileDownloadUri,
                    dbFile.getType(),
                    dbFile.getData().length);
        }).collect(Collectors.toList());
    }

    @NotNull
    public ResponseEntity<ResponseMessage> getUploadFile(MultipartFile file) {
        String message = "";
        try {
            store(file);

            message = "Uploaded the file successfully: " + file.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
        } catch (Exception e) {
            message = "Could not upload the file: " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
        }
    }

    public void uploadDocumentsToSchool(String schoolId, MultipartFile file) throws SchoolException, IOException {
        SchoolEntity school = schoolRepository.getById(schoolId);
        FileDB fileDB = store(file);
        school.setDocuments(fileDB.getId());
        schoolRepository.save(school);
    }
}
