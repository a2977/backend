package com.example.demo.services;

import com.example.demo.dto.CreateStudent;
import com.example.demo.entities.CompanyEntity;
import com.example.demo.entities.StudentCompanyEntity;
import com.example.demo.entities.StudentEntity;
import com.example.demo.exceptions.CompanyException;
import com.example.demo.exceptions.StudentException;
import com.example.demo.repositories.CompanyRepository;
import com.example.demo.repositories.FileDBRepository;
import com.example.demo.repositories.StudentCompanyRepository;
import com.example.demo.repositories.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class StudentService {
    StudentRepository studentRepository;
    CompanyRepository companyRepository;
    StudentCompanyRepository studentCompanyRepository;
    FileDBRepository fileDBRepository;

    public StudentService(StudentRepository studentRepository, CompanyRepository companyRepository, StudentCompanyRepository studentCompanyRepository) {
        this.studentRepository = studentRepository;
        this.companyRepository = companyRepository;
        this.studentCompanyRepository = studentCompanyRepository;
    }

    public Stream<StudentEntity> allStudents() {
        return studentRepository.findAll().stream();
    }

    public StudentEntity getById(String id) throws StudentException {
        return studentRepository.findById(id).orElseThrow(() -> new StudentException("Student id " + id + " not found"));
    }

    public StudentEntity createStudent(CreateStudent createStudent) {
        /* replace id with authToken() */
        //              new StudentCompanyEntity(UUID.randomUUID().toString(), new ArrayList<>(), new ArrayList<>())));
//              studentCompanyRepository.save(studentEntity.getStudentCompanyEntity());
        return studentRepository.save(new StudentEntity(
                UUID.randomUUID().toString(),
                createStudent.getName(),
                createStudent.getEmail(),
                createStudent.getCity(),
                createStudent.getCourse(),
                createStudent.getSchool(),
                null,
                null));
    }

    public void deleteStudent(String id) throws StudentException {
        StudentEntity student = getById(id);
        studentRepository.delete(student);
    }

    public Stream<CompanyEntity> searchCompanies(String searchValue) {
        return companyRepository.searchCompany(searchValue);
    }

    public StudentEntity addToFavorites(String studentId, String companyId) throws CompanyException, StudentException {
        StudentEntity student = studentRepository.findById(studentId).orElseThrow(() -> new StudentException("Student is not found"));
        CompanyEntity company = companyRepository.findById(companyId).orElseThrow(() -> new CompanyException("Company is not found"));

        student.addCompany(company);
        studentRepository.save(student);
        return student;
    }

    public StudentEntity getStudentsFavorites(String studentId) throws StudentException {
        return studentRepository.findById(studentId).orElseThrow(() -> new StudentException("Student is not found"));
    }

    public StudentCompanyEntity createNewStudentCompanyEntity(StudentEntity student, CompanyEntity company) {
        StudentCompanyEntity studentCompanyEntity = new StudentCompanyEntity(
                UUID.randomUUID().toString(),
                student,
                company
        );
        studentCompanyRepository.save(studentCompanyEntity);
        return studentCompanyEntity;
    }

    public void studentUpdate(String id, Map<String, Object> newStudentInfo) throws StudentException {
        //        studentUpdate.entrySet().forEach(stringObjectEntry -> System.out.println(stringObjectEntry.getKey() + "," + stringObjectEntry.getValue()));
        StudentEntity student = getById(id);
        student.setName((String) newStudentInfo.get("name"));
        student.setEmail((String) newStudentInfo.get("email"));
        student.setCity((String) newStudentInfo.get("city"));
        student.setCourse((String) newStudentInfo.get("course"));
        student.setSchool((String) newStudentInfo.get("school"));
        studentRepository.save(student);
    }
}
