package com.example.demo.services;

import com.example.demo.dto.CreateSchool;
import com.example.demo.entities.CompanyEntity;
import com.example.demo.entities.SchoolEntity;
import com.example.demo.entities.StudentEntity;
import com.example.demo.exceptions.SchoolException;
import com.example.demo.repositories.CompanyRepository;
import com.example.demo.repositories.SchoolRepository;
import com.example.demo.repositories.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class SchoolService {
    SchoolRepository schoolRepository;
    CompanyRepository companyRepository;
    StudentRepository studentRepository;

    public SchoolService(SchoolRepository schoolRepository, CompanyRepository companyRepository, StudentRepository studentRepository) {
        this.schoolRepository = schoolRepository;
        this.companyRepository = companyRepository;
        this.studentRepository = studentRepository;
    }

    public Stream<SchoolEntity > all() {
        return schoolRepository.findAll().stream();
    }

    public List<SchoolEntity> findAll() {
        return schoolRepository.findAll();
    }

    public SchoolEntity createSchool(CreateSchool createSchool) {
        SchoolEntity schoolEntity = new SchoolEntity(UUID.randomUUID().toString(), createSchool.getName(), createSchool.getEmail(),
               createSchool.getCity(), null);
        schoolRepository.save(schoolEntity);
       return schoolEntity;
    }

    public void deleteSchool(String id) throws SchoolException {
        SchoolEntity school = getById(id);
        schoolRepository.delete(school);
    }

    public SchoolEntity getById(String id) throws SchoolException {
        return schoolRepository.findById(id).orElseThrow(() -> new SchoolException("School id "+ id + " not found"));
    }

    public Stream<String> getCities(){
        return companyRepository.findAll().stream().map(CompanyEntity::getCity);
    }

    public Stream<String> getProfessions(){
        return studentRepository.findAll().stream().map(StudentEntity::getCourse);
    }

    public void schoolUpdate(String id, Map<String, Object> newSchoolInfo) throws SchoolException {
        SchoolEntity school = getById(id);
        school.setName((String) newSchoolInfo.get("name"));
        school.setEmail((String) newSchoolInfo.get("email"));
        school.setCity((String) newSchoolInfo.get("city"));
        schoolRepository.save(school);
    }

}