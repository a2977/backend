package com.example.demo.services;

import com.example.demo.dto.CreateCompany;
import com.example.demo.entities.CompanyEntity;
import com.example.demo.entities.StudentCompanyEntity;
import com.example.demo.entities.StudentEntity;
import com.example.demo.exceptions.CompanyException;
import com.example.demo.exceptions.StudentException;
import com.example.demo.repositories.CompanyRepository;
import com.example.demo.repositories.FileDBRepository;
import com.example.demo.repositories.StudentCompanyRepository;
import com.example.demo.repositories.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class CompanyService {
    CompanyRepository companyRepository;
    StudentRepository studentRepository;
    StudentCompanyRepository studentCompanyRepository;
    FileDBRepository fileDBRepository;

    public CompanyService(CompanyRepository companyRepository, StudentRepository studentRepository, StudentCompanyRepository studentCompanyRepository, FileDBRepository fileDBRepository) {
        this.companyRepository = companyRepository;
        this.studentRepository = studentRepository;
        this.studentCompanyRepository = studentCompanyRepository;
        this.fileDBRepository = fileDBRepository;
    }

    public Stream<CompanyEntity> getAllCompanies() {
        return companyRepository.findAll().stream();
    }

    public CompanyEntity getById(String token) throws CompanyException {
        return companyRepository.findById(token).orElseThrow(() -> new CompanyException("Company id " + token + " not found"));
    }

    public Optional<CompanyEntity> getCompanyById(String id) {
        return companyRepository.findById(id);
    }

    public CompanyEntity createCompany(CreateCompany createCompany) {
        return companyRepository.save(new CompanyEntity(
                UUID.randomUUID().toString(),
                createCompany.getName(),
                createCompany.getEmail(),
                createCompany.getProfession(),
                createCompany.getCity(),
                null));
    }

    public void companyUpdate(String id, Map<String, Object> newCompanyInfo) throws CompanyException {
        CompanyEntity company = getById(id);
        company.setName((String) newCompanyInfo.get("name"));
        company.setEmail((String) newCompanyInfo.get("email"));
        company.setProfession((String) newCompanyInfo.get("profession"));
        company.setCity((String) newCompanyInfo.get("city"));
        companyRepository.save(company);
    }

    public void deleteCompany(String id) throws CompanyException {
        CompanyEntity company = getById(id);
        companyRepository.delete(company);
    }

    public Stream<StudentEntity> searchStudents(String searchValue) {
        return studentRepository.searchStudent(searchValue);
    }

    public CompanyEntity addToFavorites(String companyId, String studentId) throws CompanyException, StudentException {
        CompanyEntity company = companyRepository.findById(companyId).orElseThrow(() -> new CompanyException("Company is not found"));
        StudentEntity student = studentRepository.findById(studentId).orElseThrow(() -> new StudentException("Student is not found"));

        company.addStudent(student);
        companyRepository.save(company);
        return company;
    }

//    public List<StudentEntity> getCompaniesFavorites(String companyId) throws CompanyException {
//        CompanyEntity company = companyRepository.findById(companyId).orElseThrow(() -> new CompanyException("Company is not found"));
//        return company.getFavoriteStudents();
//    }
public CompanyEntity getCompaniesFavorites(String companyId) throws CompanyException {
    return companyRepository.findById(companyId).orElseThrow(() -> new CompanyException("Company is not found"));
}

    public StudentCompanyEntity createNewStudentCompanyEntity(StudentEntity student, CompanyEntity company) {
        StudentCompanyEntity studentCompanyEntity = new StudentCompanyEntity(
                UUID.randomUUID().toString(),
                student,
                company
        );
        studentCompanyRepository.save(studentCompanyEntity);
        return studentCompanyEntity;
    }

}
//    public Stream<StudentEntity> getStudentByValue(String keyVal) throws StudentException {
//        return studentRepository.findAll().stream().filter(studentEntity ->
//                studentEntity.getName().toLowerCase().contains(keyVal.toLowerCase())
//                        || studentEntity.getCity().toLowerCase().contains(keyVal.toLowerCase())
//                        || studentEntity.getCourse().toLowerCase().contains(keyVal.toLowerCase())
//                        || studentEntity.getSchool().toLowerCase().contains(keyVal.toLowerCase()));
//    }
