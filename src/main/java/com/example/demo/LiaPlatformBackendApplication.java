package com.example.demo;

import com.example.demo.entities.CompanyEntity;
import com.example.demo.entities.SchoolEntity;
import com.example.demo.entities.StudentEntity;
import com.example.demo.repositories.CompanyRepository;
import com.example.demo.repositories.SchoolRepository;
import com.example.demo.repositories.StudentRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;

@SpringBootApplication
public class LiaPlatformBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(LiaPlatformBackendApplication.class, args);
    }
    @Bean
    public CommandLineRunner demoData(StudentRepository repo, CompanyRepository repo2, SchoolRepository repo3) {
        return args -> {
             repo.save(new StudentEntity("1","KungNiclas","Niclas@ÄrBäst.se","Stockholm","java","EC",null,null));
             repo.save(new StudentEntity("2","Kalle","Kalle@ÄrBäst.se","Stockholm","java","EC",null,null));
             repo.save(new StudentEntity("3","Micke","Micke@ÄrBäst.se","Stockholm","java","EC",null,null));
             repo.save(new StudentEntity("4","Pelle","Pelle@ÄrBäst.se","Stockholm","java","EC",null,null));
             repo.save(new StudentEntity("5","Caroline","Caroline@ÄrBäst.se","Uppsala","java","EC",null,null));
             repo.save(new StudentEntity("6","Diako","Diako@ÄrBäst.se","Uppsala","java","EC",null,null));
             repo.save(new StudentEntity("7","Jakob","Jakobs@ÄrBäst.se","Örebro","java","EC",null,null));
             repo.save(new StudentEntity("8","Teodor","Teodor@ÄrBäst.se","Örebro","java","EC",null,null));
             repo.save(new StudentEntity("9","Zakhida","Zakhida@ÄrBäst.se","Stockholm","java","EC",null,null));
             repo.save(new StudentEntity("10","Linnea","Linnea@ÄrBäst.se","Stockholm","java","EC",null,null));
             repo.save(new StudentEntity("11","Oscar","Oscar@ÄrBäst.se","Göteborg","JavaScript","EC",null,null));
//             repo.save(new StudentEntity("13","Mike","Mike@ÄrBäst.se","Kalmar","JavaScript","EC",null,null));
//             repo.save(new StudentEntity("14","Mike","Mike@ÄrBäst.se","Kalmar","HTML","EC",null,null));
//             repo.save(new StudentEntity("15","Mike","Mike@ÄrBäst.se","Kalmar","java","EC",null,null));
//             repo.save(new StudentEntity("16","Mike","Mike@ÄrBäst.se","Kalmar","JavaScript","EC",null,null));
//             repo.save(new StudentEntity("17","Mike","Mike@ÄrBäst.se","Kalmar","HTML","EC",null,null));
//             repo.save(new StudentEntity("18","Mike","Mike@ÄrBäst.se","Kalmar","HTML","EC",null,null));

             repo2.save(new CompanyEntity("1","Volvo","Volvo@Gmail.com", "java","Stockholm",null));
             repo2.save(new CompanyEntity("2","Google","google@gmail.com","java","Göteborg",null));
             repo2.save(new CompanyEntity("3","Amazon","Amazon@Gmail.com", "java","Stockholm",null));
             repo2.save(new CompanyEntity("4","Ica","Icae@gmail.com","java","Göteborg",null));
             repo2.save(new CompanyEntity("5","Coop","Coop@Gmail.com", "java","Stockholm",null));
             repo2.save(new CompanyEntity("6","Ebay","Ebay@gmail.com","java","Göteborg",null));
             repo2.save(new CompanyEntity("7","Tesla","Tesla@Gmail.com", "java","Stockholm",null));
             repo2.save(new CompanyEntity("8","Disney","Disney@gmail.com","java","Göteborg",null));

             repo3.save(new SchoolEntity("1","EcUtbildning","email","Stocholm",new ArrayList<>()));
             repo3.save(new SchoolEntity("2","Hällbyskolan","email","Stocholm",new ArrayList<>()));
             repo3.save(new SchoolEntity("3","NackaAkademin","email","Stocholm",new ArrayList<>()));
             repo3.save(new SchoolEntity("4","EcUtbildning","email","Stocholm",new ArrayList<>()));
             repo3.save(new SchoolEntity("5","EcUtbildning","email","Stocholm",new ArrayList<>()));
             repo3.save(new SchoolEntity("6","EcUtbildning","email","Stocholm",new ArrayList<>()));
             repo3.save(new SchoolEntity("7","EcUtbildning","email","Stocholm",new ArrayList<>()));
        };
    }
}
