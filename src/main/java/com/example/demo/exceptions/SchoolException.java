package com.example.demo.exceptions;

public class SchoolException extends Exception{
    public SchoolException(String message){
        super(message);
    }
}
