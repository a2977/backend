package com.example.demo.exceptions;

public class CompanyException extends Exception{
    public CompanyException(String message){
        super(message);
    }
}
