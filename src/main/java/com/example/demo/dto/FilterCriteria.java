package com.example.demo.dto;

import lombok.Data;

import java.util.List;

@Data
public class FilterCriteria {
    List<String> cities;
    List<String> courses;
//    List<String> companies;
}
