package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
public class CreateSchool {
    String name;
    String email;
    String city;

    @JsonCreator
    public CreateSchool(
            @JsonProperty("name") String name,
            @JsonProperty("email") String email,
            @JsonProperty("city") String city)
    {
        this.name = name;
        this.email = email;
        this.city = city;
    }
}
