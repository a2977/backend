package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class CreateStudent {
    String name;
    String email;
    String city;
    String course;
    String school;
    String imageId;
    String Cv;

    @JsonCreator
    public CreateStudent(
                         @JsonProperty("name") String name,
                         @JsonProperty("email") String email,
                         @JsonProperty("city")  String city,
                         @JsonProperty("course") String course,
                         @JsonProperty("school") String school,
                         @JsonProperty("imageId") String imageId,
                         @JsonProperty("cv")  String cv) {
        this.name = name;
        this.email = email;
        this.city = city;
        this.course = course;
        this.school = school;
        this.imageId = imageId;
        this.Cv = cv;
    }
}
