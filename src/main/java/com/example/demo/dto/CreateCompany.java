package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class CreateCompany {
    String name;
    String email;
    String profession;
    String city;

    @JsonCreator
    public CreateCompany(
            @JsonProperty("name") String name,
            @JsonProperty("email") String email,
            @JsonProperty("profession") String profession,
            @JsonProperty("city") String city) {
        this.name = name;
        this.email = email;
        this.profession = profession;
        this.city = city;
    }
}

