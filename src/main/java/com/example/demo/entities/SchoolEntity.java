package com.example.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Entity
@AllArgsConstructor
@Data
@NoArgsConstructor
public class SchoolEntity {
    @Id String id;
    String name;
    String email;
    String city;

    @ElementCollection
    List<String> documents;


    public void setDocuments(String id) {
        documents.add(id);
    }
}
