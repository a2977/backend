package com.example.demo.entities;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity(name = "company")
//@NoArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor

public class CompanyEntity {
    @Id String id;
    String name;
    String email;
    String profession;
    String city;
    String imageId;

//    @JsonIgnore
//    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL)
//    List<StudentCompanyEntity> favoriteStudents;

    @ManyToMany(cascade = CascadeType.ALL)
    List<StudentEntity> favoriteStudents;

    public CompanyEntity(String id, String name, String email, String profession,  String city, String imageId) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.profession = profession;
        this.city = city;
        this.imageId = imageId;
        this.favoriteStudents = new ArrayList<>();
    }

//    public void addStudent(StudentCompanyEntity studentCompanyEntity) {
//        getFavoriteStudents().add(studentCompanyEntity);
//    }

    public void addStudent(StudentEntity student) {
        getFavoriteStudents().add(student);
    }
}
