package com.example.demo.entities;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name = "student_company")
@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StudentCompanyEntity {

    @Id
    String id;

    @ManyToOne()
    @JoinColumn(name = "student_id", nullable = false) //?
    StudentEntity student;

    @ManyToOne()
    @JoinColumn(name = "company_id", nullable = false)
    CompanyEntity company;
}
