package com.example.demo.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "student")
@Data
@AllArgsConstructor
//@NoArgsConstructor
@NoArgsConstructor()
public class StudentEntity {
    @Id
    String id;
    String name;
    String email;
    String city;
    String course;
    String school;
    String imageId;
    String Cv;

    //    @JsonIgnore
//    @OneToMany(mappedBy = "student", cascade = CascadeType.ALL)
//    List<StudentCompanyEntity> companies;
    @ManyToMany(cascade = CascadeType.ALL)
    List<CompanyEntity> companies;

    public StudentEntity(String id, String name, String email, String city, String course, String school, String imageId, String Cv) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.city = city;
        this.course = course;
        this.school = school;
        this.imageId = imageId;
        this.Cv = Cv;
        this.companies = new ArrayList<>();
    }
//
//    public void addCompany(StudentCompanyEntity studentCompanyEntity) {
//        getCompanies().add(studentCompanyEntity);
//    }

    public void addCompany(CompanyEntity companyEntity) {
        getCompanies().add(companyEntity);
    }
}
