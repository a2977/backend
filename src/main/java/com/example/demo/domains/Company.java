package com.example.demo.domains;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.List;

@Value
public class Company {
    String id;
    String name;
    String email;
    String profession;
    String city;
    String imageId;
    List<String> favoriteStudents;

    @JsonCreator
    public Company(@JsonProperty("id") String id,
                   @JsonProperty("name") String name,
                   @JsonProperty("email") String email,
                   @JsonProperty("profession") String profession,
                   @JsonProperty("city") String city,
                   @JsonProperty("imageId") String imageId,
                   @JsonProperty("favoriteStudents") List<String> favoriteStudents
    ) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.profession = profession;
        this.city = city;
        this.imageId = imageId;
        this.favoriteStudents = favoriteStudents;
    }
}



