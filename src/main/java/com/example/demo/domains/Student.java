package com.example.demo.domains;

import com.example.demo.entities.StudentCompanyEntity;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;

import java.util.ArrayList;
import java.util.List;


@Value
public class Student {
    String id;
    String name;
    String email;
    String city;
    String course;
    String school;
    String imageId;
    String Cv;
    List<String> favorites;

    @JsonCreator
    public Student(
            @JsonProperty("id") String id,
            @JsonProperty("name") String name,
            @JsonProperty("email") String email,
            @JsonProperty("city") String city,
            @JsonProperty("course") String course,
            @JsonProperty("school") String school,
            @JsonProperty("imageId") String imageId,
            @JsonProperty("Cv") String Cv,
            @JsonProperty("favorites") List<String> favorites) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.city = city;
        this.course = course;
        this.school = school;
        this.imageId = imageId;
        this.Cv = Cv;
        this.favorites = favorites;
    }
}



