package com.example.demo.domains;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.List;

@Value
public class School {
    String id;
    String name;
    String email;
    String city;
    List<String> documents;

    @JsonCreator
    public School(
            @JsonProperty ("id") String id,
            @JsonProperty ("name") String name,
            @JsonProperty ("email") String email,
            @JsonProperty ("city") String city,
            @JsonProperty ("documents") List< String > documents) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.city = city;
        this.documents = documents;
    }
}
