package com.example.demo.entityToDto;

import com.example.demo.domains.Company;
import com.example.demo.entities.CompanyEntity;
import com.example.demo.entities.StudentEntity;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public interface CompanyEntityToDto {

    default Company companyEntityToDto(CompanyEntity companyEntity) {
        return new Company(
                companyEntity.getId(),
                companyEntity.getName(),
                companyEntity.getEmail(),
                companyEntity.getProfession(),
                companyEntity.getCity(),
                null,
                companyEntity.getFavoriteStudents().stream().map(StudentEntity::getName).collect(Collectors.toList())
                //companyEntity.getFavoriteStudents().stream().map(studentCompanyEntity -> studentCompanyEntity.getStudent().getName()).collect(Collectors.toList())
        );
    }
}
