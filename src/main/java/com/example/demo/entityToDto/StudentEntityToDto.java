package com.example.demo.entityToDto;

import com.example.demo.domains.Student;
import com.example.demo.entities.CompanyEntity;
import com.example.demo.entities.StudentEntity;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public interface StudentEntityToDto {
    default Student studentEntityToDto(StudentEntity studentEntity) {
        return new Student(
                studentEntity.getId(),
                studentEntity.getName(),
                studentEntity.getEmail(),
                studentEntity.getCity(),
                studentEntity.getCourse(),
                studentEntity.getSchool(),
                studentEntity.getImageId(),
                studentEntity.getCv(), studentEntity.getCompanies().stream()
                .map(CompanyEntity::getName).collect(Collectors.toList())
//                studentEntity.getCompanies().stream().map(
//                        studentCompanyEntity ->
//                        studentCompanyEntity.getCompany().getName())
//                        .collect(Collectors.toList())
        );
    }
}
