package com.example.demo.entityToDto;

import com.example.demo.domains.School;
import com.example.demo.entities.SchoolEntity;
import org.springframework.stereotype.Service;

@Service
public interface SchoolEntityToDto {
    default School schoolEntityToDto(SchoolEntity schoolEntity){
        return new School(
                schoolEntity.getId(),
                schoolEntity.getName(),
                schoolEntity.getEmail(),
                schoolEntity.getCity(),
                schoolEntity.getDocuments());
    }
}
