package com.example.demo.entityToDto;

import com.example.demo.entities.StudentCompanyEntity;
import org.springframework.stereotype.Service;

@Service
public interface StudentCompanyEntityToDto {
    default StudentCompanyEntity studentCompanyEntityToDto(StudentCompanyEntity studentCompanyEntity){
        return new StudentCompanyEntity(
                studentCompanyEntity.getId(),
                studentCompanyEntity.getStudent(),
                studentCompanyEntity.getCompany());
    }
}
