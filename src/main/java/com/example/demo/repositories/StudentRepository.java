package com.example.demo.repositories;

import com.example.demo.entities.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.stream.Stream;

public interface StudentRepository extends JpaRepository<StudentEntity, String> {
    @Query("SELECT p FROM student p WHERE p.name LIKE %?1%"
            + " OR p.city LIKE %?1%"
            + " OR p.course LIKE %?1%"
            + " OR p.email LIKE %?1%"
            + " OR CONCAT(p.school, '') LIKE %?1%")
    Stream<StudentEntity> searchStudent(String keyword);
}
