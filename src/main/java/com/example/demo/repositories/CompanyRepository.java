package com.example.demo.repositories;

import com.example.demo.entities.CompanyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.stream.Stream;

public interface CompanyRepository extends JpaRepository<CompanyEntity, String> {
    @Query("SELECT p FROM company p WHERE p.name LIKE %?1%"
            + " OR p.city LIKE %?1%")
    Stream<CompanyEntity> searchCompany(String keyword);

}
