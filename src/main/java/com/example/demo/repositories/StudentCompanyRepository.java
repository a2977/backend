package com.example.demo.repositories;

import com.example.demo.entities.StudentCompanyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentCompanyRepository extends JpaRepository<StudentCompanyEntity, String> {
}
